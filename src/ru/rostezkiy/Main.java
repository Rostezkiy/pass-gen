package ru.rostezkiy;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int size = 10;
        int amount = 5;
        boolean exit = false;
        Scanner userInput = new Scanner(System.in);
        System.out.println("\n 1 - generate password \n 2 - change password length (default 10) \n 3 - change amount of generated passwords \n 0 - exit");

        while (!exit) {

            String number;
            String command;
            System.out.println("Enter command: ");
            command = userInput.nextLine();
            if (!command.matches("[0-9]+")) {
                System.out.println("Enter number: ");
            }


            switch (command) {
                case "1":
                    System.out.println("Passwords: \n");
                    for (int i = 1; i <= amount; i++) {
                        String password = new Random().ints(size, 33, 122).collect(StringBuilder::new,
                                StringBuilder::appendCodePoint, StringBuilder::append)
                                .toString();
                        System.out.println(password);
                    }
                    System.out.println("\n");
                    break;
                case "2":
                    do {
                        try {
                            System.out.println("Length: ");
                            size = userInput.nextInt();
                            if (size < 1) {
                                System.out.println("Length must be > 0. ");
                            }
                        } catch (InputMismatchException e) {
                            System.out.print("Invalid number. ");
                        }
                        userInput.nextLine();
                    } while (size <= 1);
                    System.out.println("Length of password = " + size);
                    break;
                case "3":
                    do {
                        try {
                            System.out.println("Amount: ");
                            amount = userInput.nextInt();
                            if (amount < 1) {
                                System.out.println("Amount must be >0. ");
                            }
                        } catch (InputMismatchException e) {
                            System.out.print("Invalid number. ");
                        }
                        userInput.nextLine();
                    } while (amount <= 1);
                    System.out.println(amount + " password(s) will be generated.");
                    break;
                case "0":
                    System.out.println("Good Bye!");
                    exit = true;
                    break;
                default:
                    System.out.println("\n 1 - generate password \n 2 - change password length (default 10) \n 3 - change amount of generated passwords \n 0 - exit");

            }
        }
    }
}