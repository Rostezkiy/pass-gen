Simple CLI generator of strong passwords. You can choose length of the password and amount generated.
 
Usage (available commands):

* 1 - generate password 
* 2 - change password length (default 10) 
* 3 - change amount of generated passwords (default 5)
* 0 - exit 